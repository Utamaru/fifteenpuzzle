﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    public Enumerations.GameState GameState
    {
        get
        {
            return m_gameState;
        }
        set
        {
            m_gameState = value;

            switch (value)
            {
                case Enumerations.GameState.NONE:
                case Enumerations.GameState.SOLVE:
                    TextGameState.text = string.Empty;
                    break;
                case Enumerations.GameState.VICTORY:
                    TextGameState.text = "Victory";
                    break;
            }
        }
    }
    private Enumerations.GameState m_gameState = Enumerations.GameState.NONE;

    public GameObject TilePrefab;
    public Field Field;
    public AnimationCurve TileMovement;
    public bool BlockInput;
    public Camera Camera;

    public int Moves
    {
        get
        {
            return m_moves;
        }
        set
        {
            m_moves = value;
            TextMoves.text = string.Format("Moves: " + value);
        }
    }
    private int m_moves;

    #region UI
    public Text TextMoves;
    public Text TextGameState;
    #endregion

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        AdjustCameraSize(Constants.Instance.FieldDimensions);

        Field = new Field();
        Field.Initialize(Constants.Instance.FieldDimensions);
        Field.Spawn(TilePrefab);
    }

    public void NewGame()
    {
        Moves = 0;
        Field.RestoreDefaultState();
        Field.Shuffle(Constants.Instance.Shuffles);

        GameState = Enumerations.GameState.SOLVE;
    }

    private void AdjustCameraSize(int fieldSize)
    {
        Camera.orthographicSize = fieldSize * 1.75f;
    }

    public void OnTileClick(Tile tile)
    {
        if (!BlockInput && GameState == Enumerations.GameState.SOLVE)
        {
            if (Field.TryMoveTile(tile))
            {
                Moves += 1;
            }
        }
    }

    public void MoveTile(Transform t, Vector3 position)
    {
        StartCoroutine(MoveTileCoroutine(t, position));
    }

    private IEnumerator MoveTileCoroutine(Transform t, Vector3 position)
    {
        BlockInput = true;

        float time = 0.0f;
        Vector3 positionInitial = t.position;

        while (time != 1.0f)
        {
            time = Mathf.Clamp(time + Time.deltaTime * Constants.Instance.TileSpeed, 0.0f, 1.0f);
            t.position = Vector3.Lerp(positionInitial, position, TileMovement.Evaluate(time));
            yield return null;
        }

        t.position = position;

        if (Field.AreTilesInCorrectPosition())
        {
            GameState = Enumerations.GameState.VICTORY;
        }

        BlockInput = false;
    }
}
