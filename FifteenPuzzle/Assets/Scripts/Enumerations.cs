﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enumerations
{
    public enum GameState
    {
        NONE = 0,
        SOLVE = 1,
        VICTORY = 2
    }
}
