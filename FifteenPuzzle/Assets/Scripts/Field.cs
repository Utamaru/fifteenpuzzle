﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Field
{
    public Tile[,] Tiles;

    private float m_startingAxisPosition;
    private Vector2Int m_emptyTileIndex;

    public void Initialize(int size)
    {
        Tiles = new Tile[size, size];

        m_startingAxisPosition = Constants.Instance.FieldDimensions / 2.0f * Constants.Instance.TilePositionStep
            - Constants.Instance.TilePositionStep / 2.0f;
    }

    public void Spawn(GameObject tilePrefab)
    {
        int id = 0;

        float x = -m_startingAxisPosition;
        float y = x;

        for (int row = 0; row < Tiles.GetLength(1); row++)
        {
            x = -m_startingAxisPosition;
            for (int column = 0; column < Tiles.GetLength(0); column++)
            {
                Vector2Int index = new Vector2Int(row, column);
                Vector3 position = GetPositionFromIndex(index);

                if (id >= (Constants.Instance.FieldDimensions * Constants.Instance.FieldDimensions) - 1)
                {
                    m_emptyTileIndex = index;
                    continue;
                }

                GameObject tileObject = GameObject.Instantiate(tilePrefab);
                tileObject.name = id.ToString();
                Tile tile = tileObject.GetComponent<Tile>();
                Tiles[row, column] = tile;
                tile.Transform.position = position;
                tile.ID = id;
                tile.Index = index;
                tile.CorrectIndex = index;
                id += 1;

                x += Constants.Instance.TilePositionStep;
            }
            y += Constants.Instance.TilePositionStep;
        }
    }

    public void Shuffle(int steps)
    {
        Tile randomTile = GetRandomTile();
        Tile previousTile = null;

        for (int i = 0; i < steps; i++)
        {
            while (randomTile == null || (previousTile != null && randomTile == previousTile))
            {
                randomTile = GetRandomTile();
            }

            if (IsTileMoveable(randomTile))
            {
                SwapTile(randomTile);
            }

            previousTile = randomTile;
            randomTile = null;
        }

        for (int row = 0; row < Tiles.GetLength(1); row++)
        {
            for (int column = 0; column < Tiles.GetLength(0); column++)
            {
                Tile tile = Tiles[row, column];
                if (tile != null)
                {
                    tile.Transform.position = GetPositionFromIndex(tile.Index);
                }
            }
        }
    }

    private Tile GetRandomTile()
    {
        return Tiles[Random.Range(0, Tiles.GetLength(0)), Random.Range(0, Tiles.GetLength(1))];
    }

    public void RestoreDefaultState()
    {
        for (int row = 0; row < Tiles.GetLength(1); row++)
        {
            for (int column = 0; column < Tiles.GetLength(0); column++)
            {
                Tile tile = Tiles[row, column];
                if (tile != null)
                {
                    tile.Index = tile.CorrectIndex;
                    tile.Transform.position = GetPositionFromIndex(tile.CorrectIndex);
                }
            }
        }

        m_emptyTileIndex = new Vector2Int(Tiles.GetLength(1) - 1, Tiles.GetLength(0) - 1);
    }

    public bool TryMoveTile(Tile tile)
    {
        if (IsTileMoveable(tile))
        {
            Vector3 position = GetPositionFromIndex(m_emptyTileIndex);
            GameController.Instance.MoveTile(tile.Transform, position);
            SwapTile(tile);
            return true;
        }
        return false;
    }

    public bool IsTileMoveable(Tile tile)
    {
        int deltaX = Mathf.Abs(tile.Index.x - m_emptyTileIndex.x);
        int deltaY = Mathf.Abs(tile.Index.y - m_emptyTileIndex.y);

        return (deltaX == 1 && deltaY == 0) || (deltaX == 0 && deltaY == 1);
    }

    private void SwapTile(Tile tile)
    {
        Vector2Int tempIndex = m_emptyTileIndex;
        m_emptyTileIndex = tile.Index;
        tile.Index = tempIndex;
    }

    public Vector3 GetPositionFromIndex(Vector2Int index)
    {
        return -new Vector3
        (
            m_startingAxisPosition - index.y * Constants.Instance.TilePositionStep,
            m_startingAxisPosition - index.x * Constants.Instance.TilePositionStep,
            0.0f
        );
    }

    public bool AreTilesInCorrectPosition()
    {
        bool result = true;

        for (int row = 0; row < Tiles.GetLength(1); row++)
        {
            for (int column = 0; column < Tiles.GetLength(0); column++)
            {
                Tile tile = Tiles[row, column];
                if (tile != null)
                {
                    result = tile.IsInCorrectPosition();
                    if (!result)
                    {
                        return result;
                    }
                }
            }
        }
        return result;
    }
}
