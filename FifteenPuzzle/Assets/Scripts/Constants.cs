﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Constants : ScriptableObject
{
    public static Constants Instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = Resources.Load<Constants>("Constants");
            }
            return m_instance;
        }
    }
    private static Constants m_instance;

    public int FieldDimensions = 4;
    public float TilePositionStep = 2.56f;
    public float TileSpeed = 3.0f;
    public int Shuffles = 4;
}
