﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour, IPointerClickHandler
{
    public GameObject GameObject
    {
        get
        {
            if (m_gameObject == null)
            {
                m_gameObject = this.gameObject;
            }
            return m_gameObject;
        }
    }
    private GameObject m_gameObject;

    public Transform Transform
    {
        get
        {
            if (m_transform == null)
            {
                m_transform = this.GetComponent<Transform>();
            }
            return m_transform;
        }
    }
    private Transform m_transform;

    public TextMesh TextMesh
    {
        get
        {
            if (m_textMesh == null)
            {
                m_textMesh = this.GetComponentInChildren<TextMesh>();
            }
            return m_textMesh;
        }
    }
    private TextMesh m_textMesh;

    public int ID
    {
        get
        {
            return m_id;
        }
        set
        {
            m_id = value;
            TextMesh.text = (value + 1).ToString();
        }
    }
    private int m_id;

    public Vector2Int Index;
    public Vector2Int CorrectIndex;

    public void OnPointerClick(PointerEventData eventData)
    {
        GameController.Instance.OnTileClick(this);
    }

    public bool IsInCorrectPosition()
    {
        return Index == CorrectIndex;
    }
}
